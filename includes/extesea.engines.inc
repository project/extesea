<?php

/**
 * Implementation of hook_extesea_engine_info().
 */
function extesea_extesea_engine_info() {
  return array(
    t('Dictionaries and encyclopediae') => array(
      'urbandictionary' => array(
        'title' => t('Urban Dictionary'),
        'url' => 'http://www.urbandictionary.com/define.php?term=[extesea]',
      ),
      'wikibooks' => array(
        'title' => t('Wikibooks'),
        'url' => 'http://[language].wikibooks.org/wiki/Special:Search?search=[extesea]',
        'fields' => array(
          'language' => array(
            'title' => t('Language'),
            'values' => array(
              'ang' => t('Englisc'),
              'ar' => t('العربي'),
              'bg' => t('Български'),
              'ca' => t('Català'),
              'cs' => t('Česky'),
              'da' => t('Dansk'),
              'de' => t('Deutsch'),
              'el' => t('Ελληνικά'),
              'en' => t('English'),
              'eo' => t('Esperanto'),
              'es' => t('Español'),
              'et' => t('Eesti'),
              'fa' => t('فارسی'),
              'fi' => t('Suomi'),
              'fr' => t('Français'),
              'gl' => t('Galego'),
              'he' => t('עברית'),
              'hr' => t('Hrvatski'),
              'hu' => t('Magyar'),
              'ia' => t('Interlingua'),
              'id' => t('Bahasa Indonesia'),
              'is' => t('Íslenska'),
              'it' => t('Italiano'),
              'ja' => t('日本語'),
              'ka' => t('ქართული'),
              'ko' => t('한국어'),
              'la' => t('Latina'),
              'lt' => t('Lietuvių'),
              'mk' => t('Македонски'),
              'mr' => t('मराठी'),
              'nl' => t('Nederlands'),
              'no' => t('Norsk'),
              'oc' => t('Occitan'),
              'pl' => t('Polski'),
              'pt' => t('Português'),
              'ro' => t('Română'),
              'ru' => t('Русский'),
              'simple' => t('Simple English'),
              'sk' => t('Slovenčina'),
              'sl' => t('Slovenščina'),
              'sr' => t('Српски'),
              'sv' => t('Svenska'),
              'sq' => t('Shqip'),
              'ta' => t('தமிழ்'),
              'th' => t('ไทย'),
              'tl' => t('Tagalog'),
              'tr' => t('Türkçe'),
              'ur' => t('اردو'),
              'vi' => t('Tiếng Việt'),
              'zh' => t('中文'),
            ),
          ),
        ),
      ),
      'wikipedia' => array(
        'title' => t('Wikipedia'),
        'url' => 'http://[language].wikipedia.org/wiki/Special:Search?search=[extesea]',
        'fields' => array(
          'language' => array(
            'title' => t('Language'),
            'values' => array(
              'af' => t('Afrikaans'),
              'ca' => t('Català'),
              'cs' => t('Česky'),
              'da' => t('Dansk'),
              'de' => t('Deutsch'),
              'en' => t('English'),
              'eo' => t('Esperanto'),
              'es' => t('Español'),
              'fi' => t('Suomi'),
              'fr' => t('Français'),
              'fy' => t('Frysk'),
              'hu' => t('Magyar'),
              'id' => t('Bahasa Indonesia'),
              'it' => t('Italiano'),
              'ja' => t('日本語'),
              'nl' => t('Nederlands'),
              'nn' => t('Norsk (nynorsk)'),
              'no' => t('Norsk (bokmål)'),
              'oc' => t('Occitan'),
              'pl' => t('Polski'),
              'pt' => t('Português'),
              'ru' => t('Русский'),
              'simple' => t('Simple English'),
              'sk' => t('Slovenčina'),
              'sv' => t('Svenska'),
              'sq' => t('Shqip'),
              'tr' => t('Türkçe'),
              'uk' => t('Українська'),
              'vi' => t('Tiếng Việt'),
              'vo' => t('Volapük'),
              'zh' => t('中文'),
            ),
          ),
        ),
      ),
    ),
    t('General search sites') => array(
      'ask' => array(
        'title' => t('Ask'),
        'url' => 'http://ask.com/web?q=[extesea]',
      ),
      'google' => array(
        'title' => t('Google'),
        'url' => 'http://google.com/search?q=[extesea]',
      ),
      'live' => array(
        'title' => t('Live Search'),
        'url' => 'http://search.live.com/results.aspx?q=[extesea]',
      ),
      'yahoo' => array(
        'title' => t('Yahoo!'),
        'url' => 'http://search.yahoo.com/search?p=[extesea]',
      ),
    ),
    t('Media') => array(
      'flickr' => array(
        'title' => t('Flickr'),
        'url' => 'http://flickr.com/search/?q=[extesea]',
      ),
      'googlevideo' => array(
        'title' => t('Google Video'),
        'url' => 'http://video.google.com/videosearch?q=[extesea]',
      ),
      'gracenote' => array(
        'title' => t('Gracenote CD database'),
        'url' => 'http://www.gracenote.com/search/?search_type=[type]&query=[extesea]',
        'fields' => array(
          'type' => array(
            'title' => t('Type'),
            'values' => array(
              'artist' => t('Artist'),
              'album' => t('Album'),
              'track' => t('Track'),
            ),
          ),
        ),
      ),
      'imdb' => array(
        'title' => t('<abbr title="The Internet Movie Database">IMDb</abbr>'),
        'url' => 'http://www.imdb.com/find?s=[type]&q=[extesea]',
        'fields' => array(
          'type' => array(
            'title' => t('Type'),
            'values' => array(
              'all' => t('All'),
              'tt' => t('Titles'),
              'ep' => t('TV Episodes'),
              'nm' => t('Names'),
              'co' => t('Companies'),
              'kw' => t('Keywords'),
              'char' => t('Characters'),
            ),
          ),
        ),
      ),
      'lastfm' => array(
        'title' => t('Last.fm'),
        'url' => 'http://last.fm/search?m=[type]&q=[extesea]',
        'fields' => array(
          'type' => array(
            'title' => t('Type'),
            'values' => array(
              'all' => t('Music'),
              'label' => t('Label'),
              'tag' => t('Tag'),
            ),
          ),
        ),
      ),
      'youtube' => array(
        'title' => t('YouTube'),
        'url' => 'http://www.youtube.com/results?search_type=search_[type]&search_query=[extesea]',
        'fields' => array(
          'type' => array(
            'title' => t('Type'),
            'values' => array(
              'videos' => t('Videos'),
              'users' => t('Channels'),
              'playlists' => t('Playlists'),
            ),
          ),
        ),
      ),
    ),
    t('Programming') => array(
      'drupalapi' => array(
        'title' => t('Drupal <abbr title="Application Programming Interface">API</abbr> documentation'),
        'url' => 'http://api.drupal.org/api/search/[version]/[extesea]',
        'fields' => array(
          'version' => array(
            'title' => t('Version'),
            'values' => array(
              '5' => t('Drupal 5'),
              '6' => t('Drupal 6'),
              '7' => t('Drupal 7'),
            ),
          ),
        ),
      ),
      'mysqldocs' => array(
        'title' => t('MySQL documentation'),
        'url' => 'http://search.mysql.com/search?q=[extesea]&site=documentation',
      ),
      'phpnetfunctions' => array(
        'title' => t('Php.net function list'),
        'url' => 'http://php.net/[extesea]',
      ),
    ),
    t('Software') => array(
      'downloadcom' => array(
        'title' => t('CNET Downloads (download.com)'),
        'url' => 'http://www.download.com/1770-20_4-0.html?query=[extesea]&searchtype=downloads',
      ),
      'sourceforge' => array(
        'title' => t('SourceForge.net'),
        'url' => 'http://sourceforge.net/search/?words=[extesea]',
      ),
    ),
  );
}