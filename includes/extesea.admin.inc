<?php

/**
 * @File Create custom search blocks
 */

/**
 * Administer custom engines.
 *
 * @return
 *   Type: string; Two rendered forms.
 */
function extesea_admin_engines() {
  return drupal_get_form('extesea_admin_engines_add') . drupal_get_form('extesea_admin_engines_list');
}

/**
 * Administer search blocks.
 *
 * @return
 *   Type: string; Two rendered forms.
 */
function extesea_admin_blocks() {
  return drupal_get_form('extesea_admin_blocks_add') . drupal_get_form('extesea_admin_blocks_list');
}

/**
 * Form for adding custom engines.
 * 
 * @return
 *   Type: array; A Drupal form.
 */
function extesea_admin_engines_add() {
  $form['add_custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add custom search engine'),
  );
  $form['add_custom']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#element_validate' => array('extesea_validate_title'),
  );
  $form['add_custom']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The <abbr title="Uniform Resource Locator">URL</abbr> this search engine uses. <code>[extesea]</code> will be replaced by the search keywords. Example: <code>http://example.com/search?q=[extesea]</code>.'),
    '#required' => TRUE,
    '#element_validate' => array('extesea_validate_url'),
  );
  $form['add_custom']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add engine'),
  );

  return $form;
}

/**
 * Form submission handler for extesea_admin_engines_add().
 */
function extesea_admin_engines_add_submit($form, &$form_state) {
  $values = $form_state['values'];

  extesea_engine_insert($values['title'], $values['url']);
}

/**
 * List all custom and predefined engines.
 *
 * @return
 *   Type: array; A Drupal form.
 */
function extesea_admin_engines_list() {
  $engines = extesea_engines_load_custom();

  $form = array();
  foreach ($engines as $engine) {
    $form['custom']['engine_enabled_' . $engine['name']] = array(
      '#type' => 'checkbox',
      '#title' => t($engine['title']),
      '#description' => $engine['url'],
      '#default_value' => TRUE,
    );
  }
  if (count($form)) {
    $form['custom'] += array(
      '#type' => 'fieldset',
      '#title' => t('Custom engines'),
    );
    $form['custom']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete unchecked engines'),
    );
  }

  return $form;
}

/**
 * Form submission handler for extesea_admin_engines_list().
 */
function extesea_admin_engines_list_submit($form, &$form_state) {
  $values = $form_state['values'];

  $engines = extesea_engines_load_custom();
  foreach ($engines as $engine) {
    if ($values['engine_enabled_' . $engine['name']] == FALSE) {
      extesea_engine_delete($engine['name']);
    }
  }
}

/**
 * Add a new search block.
 */
function extesea_admin_blocks_add() {
  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add form'),
  );
  $form['block']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );
  $options = array();
  $count = 0;
  foreach(extesea_engines_load_all() as $section => $engines) {
    foreach ($engines as $name => $engine) {
      $options[$section][$name] = strip_tags($engine['title']);
      $count++;
    }
    $count++;
    if (isset($options[$section])) {
      uasort($options[$section], 'extesea_engines_sort');
    }
  }
  ksort($options);
  $form['block']['engines'] = array(
    '#type' => 'select',
    '#title' => t('Engines'),
    '#required' => TRUE,
    '#options' => $options,
    '#multiple' => TRUE,
    '#size' => $count > 10 ? 10 : $count,
  );
  $form['block']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add form'),
  );

  return $form;
}

/**
 * Form submission handler for extesea_admin_blocks_add().
 */
function extesea_admin_blocks_add_submit($form, &$form_state) {
  $values = $form_state['values'];

  extesea_block_insert((object) array('title' => $values['title'], 'engines' => $values['engines']));
}

/**
 * List all existing search blocks.
 */
function extesea_admin_blocks_list() {
  $blocks = extesea_block_load_all();

  $form = array();
  foreach ($blocks as $block) {
    $form['block_enabled_' . $block->delta] = array(
      '#type' => 'checkbox',
      '#default_value' => TRUE,
    );
    $form['block_title_' . $block->delta] = array(
      '#value' => t($block->title),
    );

    extesea_block_engines_load($block);
    foreach ($block->engines as $name => $engine) {
      $prefix = 'block_engines_' . $block->delta . '_' . $name . '_';
      $form[$prefix . 'title'] = array(
        '#value' => $engine['title'],
      );
      if (isset($engine['fields'])) {
        foreach ($engine['fields'] as $token => $field) {
          $field_id = $prefix . $token;
          $count = count($field['values']);
          asort($field['values']);
          $form[$field_id] = array(
            '#type' => 'fieldset',
            '#title' => $field['title'],
            '#attributes' => array(
              'class' => 'extesea-field',
            ),
          );
          $form[$field_id][$field_id . '_allowed'] = array(
            '#type' => 'select',
            '#title' => t('Allowed values'),
            '#options' => $field['values'],
            '#multiple' => TRUE,
            '#size' => $count > 5 ? 5 : $count,
            '#default_value' => $engine['field_values'][$token]['allowed'],
          );
          $form[$field_id][$field_id . '_default'] = array(
            '#type' => 'select',
            '#title' => t('Default value'),
            '#options' => $field['values'],
            '#default_value' => $engine['field_values'][$token]['default'],
          );
        }
      }
    }
  }
  if (count($form)) {
    $form['#theme'] = 'admin_blocks_list';
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update forms'),
    );
  }

  return $form;
}

/**
 * Form submission handler for extesea_admin_blocks_list().
 */
function extesea_admin_blocks_list_submit($form, &$form_state) {
  $values = $form_state['values'];

  $blocks = extesea_block_load_all();
  foreach ($blocks as $block) {
    if ($values['block_enabled_' . $block->delta] == FALSE) {
      extesea_block_delete($block);
    }
    else {
      extesea_block_engines_load($block);
      foreach ($block->engines as $name => &$engine) {
        if (isset($engine['fields'])) {
          foreach ($engine['fields'] as $token => $field) {
            $field_id = 'block_engines_' . $block->delta . '_' . $name . '_' . $token;
            $default_value = $values[$field_id . '_default'];
            $allowed = array_unique(array_merge($values[$field_id . '_allowed'], array($default_value)));
            $engine['field_values'][$token]['allowed'] = array_combine($allowed, $allowed);
            $engine['field_values'][$token]['default'] = $default_value;
          }
        }
      }
      extesea_block_update($block);
    }
  }
}

/**
 * Insert engine into the DB.
 *
 * @param $title
 *   Type: string; The engine's human-readable name.
 * @param $url
 *   Type: string; The engine's URL.
 */
function extesea_engine_insert($title, $url) {
  $id = variable_get('extesea_engine_id', 1);
  variable_set('extesea_engine_id', $id + 1);
  $name = 'extesea_engine_' . $id;
  db_query("INSERT INTO {extesea_engines} (name, title, url) VALUES ('%s', '%s', '%s')", $name, $title, $url);
}

/**
 * Delete an engine.
 *
 * @param $name
 *   Type: integer; The engine's name.
 */
function extesea_engine_delete($name) {
  db_query("DELETE FROM {extesea_engines} WHERE name = '%s'", $name);
}

/**
 * Insert block into the DB.
 *
 * @param $block
 *   Type: object; The block object to insert.
 */
function extesea_block_insert($block) {
  drupal_write_record('extesea_blocks', $block);
  $values = array();
  foreach ($block->engines as $name) {
    $engine = extesea_engine_load($name);
    $values[] = $block->delta;
    $values[] = $name;
    $field_values = array();
    if (isset($engine['fields'])) {
      foreach ($engine['fields'] as $token => $field) {
        // Since FAPI returns a select element's values as an associative array
        // where key and value is the same for each item, we do the same.
        $keys = array_keys($field['values']);
        $field_values[$token]['allowed'] = array_combine($keys, $keys);
        $field_values[$token]['default'] = $keys[0];
      }
    }
    $values[] = serialize($field_values);
    $placeholders = implode(', ', array_fill(0, count($block->engines), "(%d, '%s', '%s')"));
  }
  db_query("INSERT INTO {extesea_block_engines} (delta, name, field_values) VALUES " . $placeholders, $values);
}

/**
 * Update a block.
 *
 * @param $block
 *   Type: object; The block object to update.
 */
function extesea_block_update($block) {
  foreach ($block->engines as $name => $engine) {
    db_query("UPDATE {extesea_block_engines} SET field_values = '%s' WHERE name = '%s' AND delta = %d", serialize($engine['field_values']), $name, $block->delta);
  }
}

/**
 * Delete a block.
 *
 * @param $block
 *   Type: object; The object of the block to delete.
 */
function extesea_block_delete($block) {
  db_query("DELETE FROM {extesea_blocks} WHERE delta = %d", $block->delta);
  db_query("DELETE FROM {extesea_block_engines} WHERE delta = %d", $block->delta);
}

/**
 * Form valiation callback for a search engine's title.
 *
 * @param $element
 *   Type: array; The form element to validate.
 */
function extesea_validate_title($element) {
  if (!empty($element['#value'])) {
    $count = db_result(db_query("SELECT COUNT(*) FROM {extesea_engines} WHERE title = '%s'", $element['#value']));
    if ($count > 0) {
      form_error($element, t('A search engine with this title already exists.'));
    }
  }
}

/**
 * Form valiation callback for a search engine's URL.
 *
 * @param $element
 *   Type: array; The form element to validate.
 */
function extesea_validate_url($element) {
  if (!empty($element['#value']) && !valid_url(str_replace('[extesea]', '', $element['#value']), TRUE)) {
    form_error($element, t('Please enter a valid URL.'));
  }
  elseif (!strpos($element['#value'], '[extesea]')) {
    form_error($element, t('URLs should contain <code>[extesea]</code>.'));
  }
}

/**
 * Theme a block list.
 */
function theme_admin_blocks_list($form) {
  drupal_add_css(drupal_get_path('module', 'extesea') . '/css/extesea.admin.css');
  drupal_add_js(drupal_get_path('module', 'extesea') . '/js/extesea.admin.js');
  $blocks = extesea_block_load_all();
  $rows = array();
  foreach ($blocks as $block) {
    // Add a header for this block.
    $rows[] = array(
      'data' => array(
        array(
          'data' => drupal_render($form['block_enabled_' . $block->delta]),
        ),
        array(
          'data' => drupal_render($form['block_title_' . $block->delta]),
          'header' => TRUE,
          'id' => 'extesea-form-' . $block->delta,
        ),
        NULL,
      ),
      'class' => 'extesea-form',
    );
    // Loop through this block's engines. Display each in a separate row.
    extesea_block_engines_load($block);
    foreach ($block->engines as $name => $engine) {
      $prefix = 'block_engines_' . $block->delta . '_' . $name . '_';
      // Render this block's fields.
      $fields = '';
      if (isset($engine['fields'])) {
        foreach ($engine['fields'] as $token => $field) {
          $fields .= drupal_render($form[$prefix . $token]);
        }
      }
      $rows[] = array(
        'data' => array(
          NULL,
          drupal_render($form[$prefix . 'title']),
          $fields,
        ),
        'class' => 'extesea-engine extesea-form-' . $block->delta,
      );
    }
  }
  $header = array(
    array(
      'data' => t('Enabled'),
      'id' => 'extesea-enabled',
    ),
    array(
      'data' => t('Title'),
      'id' => 'extesea-title',
    ),
    array(
      'data' => t('Fields'),
      'id' => 'extesea-fields',
    ),
  );

  return theme('table', $header, $rows) . drupal_render($form);
}

/**
 * Alphabetically sort an array of engines per section.
 *
 * @param $engines
 *   Type: array;
 */
function extesea_engines_sort($a, $b) {
  return strcmp($a['title'], $b['title']);
}