<?php

/**
 * @File External Search API documentation.
 */

/**
 * Declare predefined search engines.
 *
 * @return
 *   A hash-flagged array of engines and their properties.
 */
function hook_extesea_engines_info() {
  // $lastfm (name), $type (token) and $all, $label and $tag (field values) are
  // strings for this specific search engine. Dollar signs should be omitted.
  return array(
    t('Media') => array(
      '$lastfm' => array(
        'title' => t('Last.fm'),
        'url' => 'http://last.fm/search?m=[type]&q=[extesea]',
        'fields' => array(
          '$type' => array(
            'title' => t('Type'),
            'values' => array(
              '$all' => t('Music'),
              '$label' => t('Label'),
              '$tag' => t('Tag'),
            ),
          ),
        ),
      ),
    ),
  );
}