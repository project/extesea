Drupal.behaviors.exteseaCollapse = function (context) {
  var timeout = null;

  $('.extesea-form th').click(function() {
    $(this).toggleClass('expanded').attr('id');
    var rows = $('.' + this.id);
    direction = Drupal.settings[this.id];
    var i = direction ? rows.size() - 1 : 0;

    // If clicked in the middle of expanding a group, stop so we can switch directions.
    if (timeout) {
      clearTimeout(timeout);
    }

    // Function to toggle an individual row according to the current direction.
    // We set a timeout of 20 ms until the next i will be shown/hidden to
    // create a sliding effect.
    function rowToggle() {
      if (direction) {
        if (i >= 0) {
          $(rows[i]).hide();
          i--;
          timeout = setTimeout(rowToggle, 20);
        }
      }
      else {
        if (i < rows.size()) {
          $(rows[i]).show();
          i++;
          timeout = setTimeout(rowToggle, 20);
        }
      }
    }

    Drupal.settings[this.id] = !Drupal.settings[this.id];

    // Kick-off the toggling upon a new click.
    rowToggle();
  });
};